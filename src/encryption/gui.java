package encryption;

	import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

	import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

	public class gui {
	       public static JTextArea input;// = new JTextArea();
	       public static JTextArea output;// = new JTextArea();
	       public static JButton encrypt = new JButton("Encrypt");
	       public static JButton decrypt = new JButton("Decrypt");
	       public static JButton clear = new JButton("Clear All");
	       public static JButton copy = new JButton("Copy");
	       public static JButton paste = new JButton("Paste");
	       public static Clipboard clipboard;
	       public static StringSelection stringSelection;
	       public static void main(String[] args) {

	              JFrame frame = new JFrame("CryptChat2.0");
	              frame.setSize(500, 400);
	              frame.setVisible(true);

	              frame.addWindowListener(new WindowAdapter() {
	                     public void windowClosing(WindowEvent e) {
	                           System.exit(0);
	                     }
	              });

	              input = new JTextArea("");
	              output = new JTextArea("");
	              input.setLineWrap(true);
	              output.setLineWrap(true);
	              input.setRows(10);
	              output.setRows(10);
	              input.setBackground(new Color(255, 200, 200));
	              output.setBackground(new Color(200, 255, 200));	             	             

	              encrypt = new JButton("Encrypt");
	              decrypt = new JButton("Decrypt");
	              clear = new JButton("Clear All");
	              copy = new JButton("Copy");
	              encrypt.setSize(150, 50);
	              decrypt.setSize(150, 50);
	              clear.setSize(150, 50);
	              copy.setSize(150,50);
	              paste.setSize(150, 50);

	              encrypt.addActionListener(new ActionListener() {
	                     public void actionPerformed(ActionEvent e) {
	                           encrypt(input.getText());
	                     }
	              });
	              decrypt.addActionListener(new ActionListener() {
	                     public void actionPerformed(ActionEvent e) {
	                           decrypt(output.getText());
	                     }
	              });
	              
	              clear.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						//FocusEvent e = null;
						input.setText("");
						output.setText("");
					}
				});
	              
	              copy.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						copyClass copy = new copyClass();
						copy.copyText(output.getText());
					}
				});
	              
//	              paste.addActionListener(new ActionListener() {
//					
//					@Override
//					public void actionPerformed(ActionEvent e) {
//						copyClass copy = new copyClass();
//						output.setText(copy.pasteText());
//					}
//				});
	              
	              JPanel subPanel = new JPanel();
	              
	              frame.add(input, BorderLayout.NORTH);
	              frame.add(encrypt, BorderLayout.WEST);
	              subPanel.add(clear);
	              subPanel.add(copy);
//	              subPanel.add(paste);
	              frame.add(subPanel, BorderLayout.CENTER);
	              frame.add(decrypt, BorderLayout.EAST);	              
	              frame.add(output, BorderLayout.SOUTH);
	              frame.setVisible(true);

	       }

	       public static String encrypt(String s) {
	              output.setText(Encrypt.encrypt(s));
	              return s;
	       }

	       public static String decrypt(String s) {
	              input.setText(Encrypt.decrypt(s));
	              return s;

	       }	       	      
}
