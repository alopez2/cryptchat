package encryption;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

public class copyClass implements ClipboardOwner {

	@Override
	public void lostOwnership(Clipboard arg0, Transferable arg1) {
		//do nothing
	}
	
	public void copyText(String textToCopy) {
		StringSelection stringSelection = new StringSelection(textToCopy);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, this);
	}
	
	public String pasteText(){
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable transferable = clipboard.getContents(null);
		String text = "";
		try{
			if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				text = (String) transferable.getTransferData(DataFlavor.stringFlavor);
				return text;
			}
		} catch (Exception e) {
			//do nothing will return ""
		}
		return text;
	}
	

}
