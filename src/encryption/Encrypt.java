package encryption;

import java.util.StringTokenizer;

public class Encrypt {

	public static String encrypt(String stringToEncrypt){
		stringToEncrypt = stringToEncrypt.toLowerCase();
		char[] stringChar = stringToEncrypt.toCharArray();
		StringBuilder builder = new StringBuilder();
		for(int i = stringChar.length-1; i >= 0; i--){
			builder.append(encrypt(stringChar[i]));
		}
		return builder.toString();
	}
	
	private static char encrypt(char letter){
		char encryptedLetter;
		int encryptedValue = 0;
		System.out.println(letter);
		int intValue = (int) letter - '`';
		
		System.out.println(intValue);
		if(intValue+'`' >= 32 && intValue+'`' <= 64){
			intValue += '`';
			return (char) intValue;
		}
		if(intValue <= 13){
			if(intValue == 1){
				encryptedValue = intValue + 25;
			}
			else{			
				encryptedValue = intValue + 12;
			}
		} else if (intValue > 13 && intValue <= 26){
			encryptedValue = intValue - 13;
		}
		encryptedValue = encryptedValue + '`';
		encryptedLetter = (char) encryptedValue;
		return encryptedLetter;
	}
	
	public static String decrypt(String stringToDecrypt){
		char[] stringChar = stringToDecrypt.toCharArray();
		StringBuilder builder = new StringBuilder();
		for(int i = stringChar.length-1; i >= 0; i--){
			builder.append(decrypt(stringChar[i]));
		}
		return builder.toString();
	}
	
	private static char decrypt(char letter){
		char encryptedLetter;
		int encryptedValue = 0;
		System.out.println(letter);
		int intValue = (int) letter - '`';
		
		System.out.println(intValue);
		if(intValue+'`' >= 32 && intValue+'`' <= 64){
			intValue += '`';
			return (char) intValue;
		}
		if(intValue <= 13){
		encryptedValue = intValue + 13;
		} else if(intValue == 26){
			encryptedValue = intValue - 25;
		} else if (intValue > 13 && intValue <= 26){
			encryptedValue = intValue - 12;
		}
		encryptedValue = encryptedValue + '`';
		encryptedLetter = (char) encryptedValue;

		return encryptedLetter;
	}

}
